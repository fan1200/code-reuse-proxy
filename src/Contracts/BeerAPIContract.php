<?php

namespace Contracts;


interface BeerAPIContract
{
    public function getBeers();
    public function getBeerImage($beerId);
}