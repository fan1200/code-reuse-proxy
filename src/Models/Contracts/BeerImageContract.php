<?php

namespace Models\Contracts;


interface BeerImageContract
{
    public function getImage();

    public function setBeerId($beerId);
}