<?php

namespace Models\Contracts;


interface BeerContract
{
    public function setId($id);
    public function setName($name);

    public function getName();
    public function getImage();
}