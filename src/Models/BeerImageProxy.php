<?php

namespace Models;


use Models\Contracts\BeerImageContract;

class BeerImageProxy implements Contracts\BeerImageContract
{
    /**
     * @var BeerImageContract
     */
    protected $beerImage = null;
    protected $beerId;

    public function getImage()
    {
        if($this->beerImage === null) {
            $this->beerImage = new BeerImage(new \BeerAPI(), $this->beerId);
        }

        return $this->beerImage->getImage();
    }

    public function setBeerId($beerId)
    {
        $this->beerId = $beerId;
    }
}