<?php

namespace Models;


class Beer implements Contracts\BeerContract
{

    protected $id;
    protected $name;
    protected $beerImage;

    public function __construct($id, $name)
    {
        $this->beerImage = new BeerImageProxy();

        $this->setId($id);
        $this->setName($name);
    }

    public function setId($id)
    {
        $this->id = $id;
        $this->beerImage->setBeerId($id);
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getImage()
    {
        return $this->beerImage->getImage();
    }
}