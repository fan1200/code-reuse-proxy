<?php

namespace Models;


use Contracts\BeerAPIContract;

class BeerImage implements Contracts\BeerImageContract
{
    protected $beerId;
    protected $image;

    /**
     * @var BeerAPIContract;
     */
    protected $beerAPI;

    public function __construct(BeerAPIContract $beerAPI, $beerId)
    {
        $this->setBeerId($beerId);
        $this->image = $beerAPI->getBeerImage($this->beerId);
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setBeerId($beerId)
    {
        $this->beerId = $beerId;
    }
}