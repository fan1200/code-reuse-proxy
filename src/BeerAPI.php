<?php


use Models\Beer;

class BeerAPI implements Contracts\BeerAPIContract
{

    public function getBeers()
    {
        $beers = [];

        $beers[] = new Beer(1, 'Heineken');
        $beers[] = new Beer(44, 'Hertog Jan');
        $beers[] = new Beer(88, 'Brand');
        $beers[] = new Beer(23, 'Amstel');
        $beers[] = new Beer(89, 'Bavaria');

        return $beers;
    }

    public function getBeerImage($beerId)
    {
        sleep(3);
        return uniqid();
    }
}