<?php

require_once("Autoloader.php");

$beerAPI = new BeerAPI();

$beers = $beerAPI->getBeers();

echo '------------- Beer list (' . count($beers) . ') -----------------' . PHP_EOL;

foreach($beers as $key => $beer) {
    echo 'Name: ' . $beer->getName() . PHP_EOL;
    if($key < 3) {
        echo $beer->getImage() . PHP_EOL;
    }

    echo PHP_EOL;
}