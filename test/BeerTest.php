<?php
require_once("Autoloader.php");

use Models\Beer;
use Models\Contracts\BeerContract;
use PHPUnit\Framework\TestCase;


class BeerTest extends TestCase
{

    /**
     * @var BeerContract
     */
    protected $beer;

    public function setUp()
    {
        parent::setUp();
        $this->beer = new Beer(1, 'Hertog Jan');
    }

    public function testBeerClassShouldExists()
    {
        $this->assertInstanceOf(Beer::class, $this->beer);
    }

    public function testBeerClassShouldBeInstanceOfBeerContract()
    {
        $this->assertInstanceOf(BeerContract::class, $this->beer);
    }
}