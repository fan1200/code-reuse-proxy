<?php
require_once("Autoloader.php");

use Models\BeerImageProxy;
use Models\Contracts\BeerImageContract;
use PHPUnit\Framework\TestCase;

class BeerImageProxyTest extends TestCase
{
    /**
     * @var BeerImageContract
     */
    protected $beerImage;

    public function setUp()
    {
        parent::setUp();
        $this->beerImage = new BeerImageProxy();
    }

    public function testBeerImageClassShouldExists()
    {
        $this->assertInstanceOf(BeerImageProxy::class, $this->beerImage);
    }

    public function testBeerImageShouldBeInstanceOfBeerImageContract()
    {
        $this->assertInstanceOf(BeerImageContract::class, $this->beerImage);
    }

}