<?php
require_once("Autoloader.php");

use Models\BeerImage;
use Models\Contracts\BeerImageContract;
use PHPUnit\Framework\TestCase;

class BeerImageTest extends TestCase
{
    /**
     * @var BeerImage
     */
    protected $beerImage;

    public function setUp()
    {
        parent::setUp();
        $beerApiDouble = Mockery::mock(BeerAPI::class);
        $beerApiDouble->shouldReceive('getBeerImage')->andReturn('testImage');
        $this->beerImage = new BeerImage($beerApiDouble, 2);
    }

    public function testBeerImageClassShouldExists()
    {
        $this->assertInstanceOf(BeerImage::class, $this->beerImage);
    }

    public function testBeerImageShouldBeInstanceOfBeerImageContract()
    {
        $this->assertInstanceOf(BeerImageContract::class, $this->beerImage);
    }

    public function testBeerImageShouldBeFilled()
    {
        $this->assertNotEmpty($this->beerImage->getImage());
    }

    public function testBeerImageHasCertainValue()
    {
        $this->assertEquals('testImage', $this->beerImage->getImage());
    }

}